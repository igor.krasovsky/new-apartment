FROM python:3.8
ENV LIBRARY_PATH=/lib:/usr/lib
WORKDIR code/

RUN pip install poetry
ADD pyproject.toml poetry.lock ./
RUN poetry config virtualenvs.create false
RUN poetry install --no-interaction --no-dev

COPY . .
