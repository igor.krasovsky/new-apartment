import pytest

from apartments.serializers import ApartmentSerializer, AddressSerializer
from apartments.models import Apartment, Address


@pytest.mark.django_db(True)
class TestResponseStatusCodeAddress:
    def test_not_authorized(self, client):
        assert client.get('/addresses/').status_code == 401

    def test_get_list(self, client, user, address_list):
        client.force_authenticate(user=user)
        response = client.get('/addresses/')
        for i, j in zip(address_list, response.data):
            assert dict(AddressSerializer(i).data) == dict(j)
        assert response.status_code == 200
        assert len(response.data) == len(address_list)

    def test_get_detail(self, client, user, address):
        client.force_authenticate(user=user)
        response = client.get(f'/addresses/{address.pk}/')
        assert response.status_code == 200
        assert dict(AddressSerializer(address).data) == dict(response.data)

    def test_post_list(self, client, user, address_data):
        client.force_authenticate(user=user)
        response = client.post('/addresses/', address_data, format='json')
        assert response.status_code == 201
        assert Address.objects.filter(**address_data).count() == 1

    def test_put_detail(self, client, user, address, address_data):
        client.force_authenticate(user=user)
        response = client.put(
            f'/addresses/{address.pk}/', address_data, format='json'
        )
        assert response.status_code == 200
        assert Address.objects.filter(**address_data).count() == 1

    def test_patch_detail(self, client, user, address, address_data):
        client.force_authenticate(user=user)
        response = client.patch(
            f'/addresses/{address.pk}/', address_data, format='json'
        )
        assert response.status_code == 200
        assert Address.objects.filter(**address_data).count() == 1


@pytest.mark.django_db
class TestResponseStatusCodeApartment:
    def test_not_authorized(self, client):
        assert client.get('/apartments/').status_code == 401

    def test_get_list(self, client, user, apartment_list):
        client.force_authenticate(user=user)
        response = client.get('/apartments/')
        for i, j in zip(apartment_list, response.data):
            assert dict(ApartmentSerializer(i).data) == dict(j)
        assert response.status_code == 200
        assert len(response.data) == len(apartment_list)

    def test_get_detail(self, client, user, apartment):
        client.force_authenticate(user=user)
        response = client.get(f'/apartments/{apartment.pk}/')
        assert response.status_code == 200
        assert dict(ApartmentSerializer(apartment).data) == dict(response.data)

    def test_post_list(self, client, user, apartment_data):
        client.force_authenticate(user=user)
        response = client.post('/apartments/', apartment_data, format='json')
        assert response.status_code == 201
        assert Apartment.objects.filter(**apartment_data).count() == 1

    def test_put_detail(self, client, user, apartment, apartment_data):
        client.force_authenticate(user=user)
        response = client.put(
            f'/apartments/{apartment.pk}/', apartment_data, format='json'
        )
        assert response.status_code == 200
        assert Apartment.objects.filter(**apartment_data).count() == 1

    def test_patch_detail(self, client, user, apartment, apartment_data):
        client.force_authenticate(user=user)
        response = client.patch(
            f'/apartments/{apartment.pk}/', apartment_data, format='json'
        )
        assert response.status_code == 200
        assert Apartment.objects.filter(**apartment_data).count() == 1
