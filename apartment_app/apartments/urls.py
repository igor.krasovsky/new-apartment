from rest_framework.routers import DefaultRouter
from django.conf.urls import include
from django.urls import path

from apartments.views import ApartmentViewSet, AddressViewSet


router = DefaultRouter()
router.register(r'apartments', ApartmentViewSet, basename='apartments')
router.register(r'addresses', AddressViewSet, basename='addresses')

urlpatterns = [path('', include(router.urls))]
