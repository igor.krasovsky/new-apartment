from django.contrib import admin

from apartments.models import Apartment, Photo, Address


admin.site.register(Apartment)
admin.site.register(Photo)
admin.site.register(Address)
