from rest_framework import viewsets

from apartments.serializers import ApartmentSerializer, AddressSerializer
from apartments.models import Apartment, Address


class ApartmentViewSet(viewsets.ModelViewSet):
    queryset = Apartment.objects.all()
    serializer_class = ApartmentSerializer


class AddressViewSet(viewsets.ModelViewSet):
    queryset = Address.objects.all()
    serializer_class = AddressSerializer
