from factory.django import DjangoModelFactory
from factory.fuzzy import FuzzyInteger, FuzzyFloat, FuzzyText
from factory import DictFactory, SubFactory, lazy_attribute

from apartments.models import Apartment, Address


class AddressFactory(DjangoModelFactory):
    class Meta:
        model = Address

    town = FuzzyText()
    street = FuzzyText()
    house = FuzzyText()
    latitude = FuzzyFloat(-90, 90)
    longitude = FuzzyFloat(-180, 180)


class ApartmentFactory(DjangoModelFactory):
    class Meta:
        model = Apartment

    price = FuzzyText()
    floor = FuzzyInteger(1, 50)
    address = SubFactory(AddressFactory)
    building_year = FuzzyInteger(1900, 2021)
    square = FuzzyFloat(20, 1000)
    rooms = FuzzyInteger(1, 6)
    description = FuzzyText()
    bathroom = FuzzyText()
    sale_conditions = FuzzyText()


class AddressDictFactory(DictFactory):
    town = FuzzyText()
    street = FuzzyText()
    house = FuzzyText()
    latitude = FuzzyFloat(-90, 90)
    longitude = FuzzyFloat(-180, 180)


class ApartmentDictFactory(DictFactory):
    price = FuzzyText()
    floor = FuzzyInteger(1, 50)
    building_year = FuzzyInteger(1900, 2021)
    square = FuzzyFloat(20, 1000)
    rooms = FuzzyInteger(1, 6)
    description = FuzzyText()
    bathroom = FuzzyText()
    sale_conditions = FuzzyText()

    @lazy_attribute
    def address(self):
        data = AddressFactory.create()
        return data.id
