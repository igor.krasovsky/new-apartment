from django.db import models
from django.utils import timezone


def get_photo_url(instance, filename):
    return '/images/apartment/{0}'.format(filename)


class Address(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    town = models.CharField(max_length=50)
    street = models.CharField(max_length=50)
    house = models.CharField(max_length=20)
    rating = models.FloatField(blank=True, null=True)

    class Meta:
        unique_together = (
            'town',
            'street',
            'house',
        )

    def __repr__(self):
        return ', '.join([str(self.town), str(self.street), str(self.house)])


class Apartment(models.Model):
    address = models.OneToOneField(Address, on_delete=models.CASCADE)
    price = models.CharField(max_length=20)
    building_year = models.IntegerField(blank=True, null=True)
    floor = models.IntegerField()
    square = models.FloatField(blank=True, null=True)
    rooms = models.IntegerField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    bathroom = models.CharField(max_length=50, blank=True, null=True)
    sale_conditions = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)

    def __repr__(self):
        return self.description


class Photo(models.Model):
    apartment = models.ForeignKey(
        Apartment, on_delete=models.CASCADE, related_name='photos'
    )
    image = models.ImageField(upload_to=get_photo_url)

    def __repr__(self):
        return self.image.upload_to
