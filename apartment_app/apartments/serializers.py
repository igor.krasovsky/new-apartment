from rest_framework import serializers

from apartments.models import Apartment, Address


class ApartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Apartment
        exclude = ('created_at', 'updated_at')


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        exclude = ('rating',)
