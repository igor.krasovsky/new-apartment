import pytest
from django.contrib.auth.models import User

from users.serializers import UserSerializer


@pytest.mark.django_db
class TestResponseStatusUser:
    def test_not_authorized(self, client):
        assert client.get('/users/').status_code == 401

    def test_get_list(self, client, user, user_list):
        client.force_authenticate(user=user)
        response = client.get('/users/')
        user_list.insert(0, user)
        for i, j in zip(user_list, response.data):
            assert dict(UserSerializer(i).data) == dict(j)
        assert response.status_code == 200
        assert len(response.data) == len(user_list)

    def test_get_detail(self, client, user):
        client.force_authenticate(user=user)
        response = client.get(f'/users/{user.pk}/')
        assert response.status_code == 200
        assert dict(UserSerializer(user).data) == dict(response.data)

    def test_post_list(self, client, user, user_data):
        client.force_authenticate(user=user)
        response = client.post('/users/', user_data, format='json')
        assert response.status_code == 201
        assert User.objects.filter(**user_data).count() == 1

    def test_put_detail(self, client, user, user_data):
        client.force_authenticate(user=user)
        response = client.put(f'/users/{user.pk}/', user_data, format='json')
        assert response.status_code == 200
        assert User.objects.filter(**user_data).count() == 1

    def test_patch_detail(self, client, user, user_data):
        client.force_authenticate(user=user)
        response = client.patch(f'/users/{user.pk}/', user_data, format='json')
        assert response.status_code == 200
        assert User.objects.filter(**user_data).count() == 1
