from rest_framework.routers import DefaultRouter
from django.conf.urls import include
from django.urls import path

from users.views import UserViewSet


router = DefaultRouter()
router.register(r'users', UserViewSet, basename='users')

urlpatterns = [path('', include(router.urls))]
