from factory.django import DjangoModelFactory
from factory.fuzzy import FuzzyText
from factory import DictFactory
from django.contrib.auth.models import User


class UserFactory(DjangoModelFactory):
    username = FuzzyText()

    class Meta:
        model = User


class UserDictFactory(DictFactory):
    username = FuzzyText()
