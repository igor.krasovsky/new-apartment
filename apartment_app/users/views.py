from rest_framework import viewsets
from django.contrib.auth.models import User
from rest_framework.response import Response

from users.serializers import UserSerializer
from users.tasks import send_registration_mail


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def perform_create(self, serializer):
        serializer.save()
        send_registration_mail(serializer.data['email'])
