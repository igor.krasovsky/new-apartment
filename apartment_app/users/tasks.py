from apartment.celery import app

from services import send


# @app.task
def send_registration_mail(email: str):
    send('Registration email', 'REG', email)
