import pytest
from rest_framework.test import APIClient

from apartment_app.apartments.factories import (
    AddressFactory,
    ApartmentFactory,
    AddressDictFactory,
    ApartmentDictFactory,
)
from apartment_app.users.factories import UserFactory, UserDictFactory
from apartment_app.apartment_filters.factories import FilterFactory


@pytest.fixture
def user():
    return UserFactory()


@pytest.fixture
def user_list():
    return UserFactory.create_batch(10)


@pytest.fixture
def apartment():
    return ApartmentFactory()


@pytest.fixture
def apartment_list():
    return ApartmentFactory.create_batch(10)


@pytest.fixture
def address():
    return AddressFactory()


@pytest.fixture
def address_list():
    return AddressFactory.create_batch(10)


@pytest.fixture
def apartment_filter():
    return FilterFactory()


@pytest.fixture
def filter_list():
    return FilterFactory.create_batch(10)


@pytest.fixture
def client():
    return APIClient()


@pytest.fixture
def address_data():
    return AddressDictFactory()


@pytest.fixture
def apartment_data():
    return ApartmentDictFactory()


@pytest.fixture
def user_data():
    return UserDictFactory()


@pytest.fixture
def filter_data():
    return {'data': {'price': '200 BYN'}}
