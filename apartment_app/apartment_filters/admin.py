from django.contrib import admin

from apartment_filters.models import ApartmentFilter

admin.site.register(ApartmentFilter)
