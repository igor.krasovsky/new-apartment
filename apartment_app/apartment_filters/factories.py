from factory.django import DjangoModelFactory
from factory import DictFactory

from apartment_filters.models import ApartmentFilter


class FilterFactory(DjangoModelFactory):
    class Meta:
        model = ApartmentFilter

    data = DictFactory.build()
