from rest_framework import viewsets

from apartment_filters.serializers import ApartmentFilterSerializer
from apartment_filters.models import ApartmentFilter


class ApartmentFilterViewSet(viewsets.ModelViewSet):
    queryset = ApartmentFilter.objects.all()
    serializer_class = ApartmentFilterSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user.id)
