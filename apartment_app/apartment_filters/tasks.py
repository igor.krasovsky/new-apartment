import json

from apartment.celery import app

from services import send
from apartment_filters.models import ApartmentFilter
from apartments.models import Apartment


@app.task
def send_email_filter_beat():
    items = ApartmentFilter.objects.all()
    for item in items:
        objects = Apartment.objects.filter(**item.data)
        send(json.dumps(objects), 'Filtered Data', item.user.email)
