from rest_framework.routers import DefaultRouter
from django.conf.urls import include
from django.urls import path

from apartment_filters.views import ApartmentFilterViewSet


router = DefaultRouter()
router.register(r'apartment_filters', ApartmentFilterViewSet)

urlpatterns = [path('', include(router.urls))]
