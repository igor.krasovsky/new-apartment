from django.db import models
from django.contrib.auth.models import User


class ApartmentFilter(models.Model):
    data = models.JSONField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
