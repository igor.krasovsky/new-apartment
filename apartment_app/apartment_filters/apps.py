from django.apps import AppConfig


class ApartmentFiltersConfig(AppConfig):
    name = 'apartment_filters'
