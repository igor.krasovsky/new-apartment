import pytest

from apartment_filters.serializers import ApartmentFilterSerializer
from apartment_filters.models import ApartmentFilter


@pytest.mark.django_db
class TestResponseStatusCodeFilter:
    def test_not_authorized(self, client):
        assert client.get('/apartment_filters/').status_code == 401

    def test_get_list(self, client, user, filter_list):
        client.force_authenticate(user=user)
        response = client.get('/apartment_filters/')
        for i, j in zip(filter_list, response.data):
            assert dict(ApartmentFilterSerializer(i).data) == dict(j)
        assert response.status_code == 200
        assert len(response.data) == len(filter_list)

    def test_get_detail(self, client, user, apartment_filter):
        client.force_authenticate(user=user)
        response = client.get(f'/apartment_filters/{apartment_filter.pk}/')
        assert response.status_code == 200
        assert dict(ApartmentFilterSerializer(apartment_filter).data) == dict(
            response.data
        )

    def test_post_list(self, client, user, filter_data):
        client.force_authenticate(user=user)
        response = client.post(
            '/apartment_filters/', filter_data, format='json'
        )
        assert response.status_code == 201
        assert ApartmentFilter.objects.filter(**filter_data).count() == 1

    def test_put_detail(self, client, user, apartment_filter, filter_data):
        client.force_authenticate(user=user)
        response = client.put(
            f'/apartment_filters/{apartment_filter.pk}/',
            filter_data,
            format='json',
        )
        assert response.status_code == 200
        assert ApartmentFilter.objects.filter(**filter_data).count() == 1

    def test_patch_detail(self, client, user, apartment_filter, filter_data):
        client.force_authenticate(user=user)
        response = client.patch(
            f'/apartment_filters/{apartment_filter.pk}/',
            filter_data,
            format='json',
        )
        assert response.status_code == 200
        assert ApartmentFilter.objects.filter(**filter_data).count() == 1
