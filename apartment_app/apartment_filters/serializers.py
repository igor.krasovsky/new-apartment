from rest_framework import serializers

from apartment_filters.models import ApartmentFilter


class ApartmentFilterSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApartmentFilter
        fields = '__all__'
