import os

from celery import Celery
from celery.schedules import crontab


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'apartment.settings')

app = Celery('application')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'send-filtered': {
        'task': 'send_email_filter_beat',
        'schedule': crontab(minute='*/1'),
    }
}
