import os
from pathlib import Path

import environ


def get_environ():
    env = environ.Env()
    base_dir = Path(__file__).resolve().parent.parent
    env_path = os.path.join(base_dir, '.env')
    environ.Env.read_env(env_file=env_path)
    return env
