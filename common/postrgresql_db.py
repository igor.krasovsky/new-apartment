from sqlalchemy import create_engine

from common.env_reader import get_environ


class PostgreSQLDB:
    def __init__(self):
        env = get_environ()
        self._connection_string = env.str('DATABASE_OPTIONS')
        self._db = create_engine(self._connection_string)

    def get_engine(self):
        return self._db
