from pika import BlockingConnection, URLParameters
from pika.adapters.blocking_connection import BlockingChannel

from common.env_reader import get_environ


class RabbitMQConnector:
    def __init__(self):
        env = get_environ()
        self._url = env.str('RABBITMQ_CONNECTION_URL')
        self._exchange = env.str('RABBITMQ_EXCHANGE')
        self._routing_key = env.str('RABBITMQ_ROUTING_KEY')
        self._queue = env.str('RABBITMQ_QUEUE')
        self._connection = self.get_connection()
        self._channel = self.declare_channel()

    def get_connection(self) -> BlockingConnection:
        parameters = URLParameters(self._url)
        return BlockingConnection(parameters=parameters)

    def declare_channel(self) -> BlockingChannel:
        rabbitmq_channel = self._connection.channel()
        rabbitmq_channel.exchange_declare(
            exchange=self._exchange, exchange_type='direct', durable=True
        )
        rabbitmq_channel.queue_declare(queue=self._queue, durable=True)
        rabbitmq_channel.queue_bind(
            queue=self._queue,
            exchange=self._exchange,
            routing_key=self._routing_key,
        )
        return rabbitmq_channel

    def channel_consume(self, callback) -> None:
        self._channel.basic_consume(
            queue=self._queue, on_message_callback=callback, auto_ack=True
        )
        self._channel.start_consuming()

    def channel_publish(self, data: str) -> None:
        self._channel.basic_publish(
            exchange=self._exchange,
            routing_key=self._routing_key,
            body=data,
        )

    def close(self) -> None:
        self._channel.close()
        self._connection.close()
