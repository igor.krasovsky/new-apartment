from redis import Redis

from common.env_reader import get_environ


class RedisConnector:
    def __init__(self):
        env = get_environ()
        self._client = Redis.from_url(env.str('REDIS_CACHE_URL'))

    def push(self, value, expiration_time):
        key = hash(value)
        self._client.set(key, value, ex=expiration_time)

    def get(self, key):
        return self._client.get(key)
