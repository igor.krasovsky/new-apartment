from sqlalchemy import (
    Table,
    Column,
    Integer,
    Float,
    String,
    Text,
    TIMESTAMP,
    ForeignKey,
    MetaData,
    UniqueConstraint,
)


metadata = MetaData()


Address = Table(
    'apartments_address',
    metadata,
    Column('id', Integer, primary_key=True),
    Column('longitude', Float, nullable=False),
    Column('latitude', Float, nullable=False),
    Column('town', String(50), nullable=False),
    Column('street', String(50), nullable=False),
    Column('house', String(20), nullable=False),
    Column('rating', Float, nullable=True),
    UniqueConstraint('town', 'street', 'house'),
)


Apartment = Table(
    'apartments_apartment',
    metadata,
    Column('id', Integer, primary_key=True),
    Column('price', String(20), nullable=False),
    Column('building_year', Integer, nullable=True),
    Column('floor', Integer, nullable=False),
    Column('square', Float, nullable=True),
    Column('rooms', Integer, nullable=True),
    Column('description', Text, nullable=True),
    Column('bathroom', String(50), nullable=True),
    Column('sale_conditions', String(50), nullable=True),
    Column('created_at', TIMESTAMP, nullable=False),
    Column('updated_at', TIMESTAMP, nullable=False),
    Column(
        'address_id',
        Integer,
        ForeignKey('apartments_address.id'),
        nullable=False,
        unique=True,
    ),
)


Photo = Table(
    'apartments_photo',
    metadata,
    Column('id', Integer, primary_key=True),
    Column('image', String(100), nullable=False),
    Column(
        'apartment_id',
        Integer,
        ForeignKey('apartments_apartment.id'),
        nullable=False,
    ),
)
