from common.redis_connector import RedisConnector
from common.env_reader import get_environ


env = get_environ()


def test_push():
    value = 'Some string'
    key = hash(value)
    r = RedisConnector()
    r.push(value, env.int('REDIS_EXPIRATION_TIME') * 3600)
    assert r.get(key).decode('utf-8') == value
