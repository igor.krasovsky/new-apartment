# Apartment

### Description
This project takes data from 3 popular apartment sites and provides api for them.:
* realt.by
* domovta.by
* onliner.by  

Also, it has filter module, that provides you an opportunity to get apartment data to your email according to filters you have posted.
To see data and get filtered to your email, you need to be authenticated. Authentication is provided by JWT.
### Technologies
* Python(Django)
* DB: PostgreSQL
* Cache: Redis
* Queue Manager: RabbitMQ


