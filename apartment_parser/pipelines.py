from json import dumps

from common.rabbitmq_connector import RabbitMQConnector


class RabbitMQItemPublisherPipeline(object):
    def __init__(self):
        self._connector = RabbitMQConnector()

    def close_spider(self, spider):
        self._connector.close()

    def process_item(self, item, spider):
        data = dumps(item.__dict__)
        self._connector.channel_publish(data)
        return item
