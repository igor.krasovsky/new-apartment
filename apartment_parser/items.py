from dataclasses import dataclass


@dataclass
class ApartmentItem:
    town: str = None
    street: str = None
    house: str = None
    floor: int = None
    price: str = None
    building_year: int = None
    square: floor = None
    rooms: int = None
    description: str = None
    photos: str = None
    bathroom: str = None
    sale_conditions: str = None
