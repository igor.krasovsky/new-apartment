import pytest

from apartment_parser.spiders.onliner.getters import (
    get_description,
    get_address,
    get_photo_url,
    get_building_year,
    remove_next_line_symbol,
)


def test_get_photo_url():
    photo_style = (
        'background-image: url(https://content.onliner.by/'
        'apartment_for_sale/2326298/600x400/'
        '177c2f78495f3763769dfa7be64f9e9e.jpeg)'
    )
    photo_url = (
        'https://content.onliner.by/'
        'apartment_for_sale/2326298/600x400/'
        '177c2f78495f3763769dfa7be64f9e9e.jpeg'
    )
    assert get_photo_url(photo_style) == photo_url


@pytest.mark.parametrize(
    'address_line, result',
    [
        ('Минск, Притыцкого, 28', ('Минск', 'Притыцкого', '28')),
        ('Колодищи, 28', (None, None, None)),
        ('Колодищи', (None, None, None)),
    ],
)
def test_get_address(address_line, result):
    assert get_address(address_line) == result


@pytest.mark.parametrize(
    'value, result', [('Хороший дом 1982 года', 1982), ('Дом', None)]
)
def test_get_building_year(value, result):
    assert get_building_year(value) == result


@pytest.mark.parametrize(
    'tag, result',
    [
        ('<div> My<br> Description </div>', 'My Description'),
        ('<div> \nMy <br>Description \n </div>', 'My Description'),
    ],
)
def test_get_description(tag, result):
    assert get_description(tag) == result


@pytest.mark.parametrize(
    'string, result', [('\n abc \n', ' abc '), (' \n abc \n ', '  abc  ')]
)
def test_remove_next_line_symbol(string, result):
    assert remove_next_line_symbol(string) == result
