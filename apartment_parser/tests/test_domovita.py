import pytest

from apartment_parser.spiders.domovita.getters import (
    get_address,
    get_description,
)
from apartment_parser.spiders.domovita.setters import add_to_item
from apartment_parser.items import ApartmentItem


@pytest.mark.parametrize(
    'address_extracted, street, house',
    [
        (['пр-т Партизанский', ', д. 87 '], 'пр-т Партизанский', '87'),
        (['\nул. Сторожовская, д. 6 '], 'ул. Сторожовская', '6'),
    ],
)
def test_get_address(address_extracted, street, house):
    assert get_address(address_extracted) == (street, house)


@pytest.mark.parametrize(
    'description_extracted, result',
    [(['My description'], 'My description'), ([], None)],
)
def test_get_description(description_extracted, result):
    assert get_description(description_extracted) == result


def test_add_to_item():
    item = ApartmentItem()
    add_to_item(item, 'price', '20000 BYN')
    assert item.price == '20000 BYN'
