import pytest

from apartment_parser.spiders.realt.getters import get_address
from apartment_parser.spiders.realt import formatters, setters
from apartment_parser.items import ApartmentItem


def test_format_town():
    assert formatters.format_town(' г.Минск ') == 'Минск'


def test_format_floor():
    assert formatters.format_floor(' 1 / 5') == 1


@pytest.mark.parametrize('rooms_number, result', [('3/5', 3), ('item', None)])
def test_format_room(rooms_number, result):
    assert formatters.format_rooms(rooms_number) == result


@pytest.mark.parametrize(
    'square, result', [('5/2/1', 5.0), ('5.4/2/1', 5.4), ('-/-/-', None)]
)
def test_format_square(square, result):
    assert formatters.format_square(square) == result


@pytest.mark.parametrize(
    'row_field, result',
    [
        (['', 'Притыцкого ул.', 'д.28'], ('Притыцкого', '28')),
        (['д.2'], (None, '2')),
        ([' Притыцкого ул., д.28'], ('Притыцкого', '28')),
    ],
)
def test_get_address(row_field, result):
    assert get_address(row_field) == result


def test_add_to_item():
    item = ApartmentItem()
    setters.add_to_item(item, 'price', '10000 USD')
    assert item.price == '10000 USD'
