# -*- coding: utf-8 -*-

from urllib.parse import urljoin
from typing import Generator

from scrapy.spiders import Spider
from scrapy.http.response.text import TextResponse

from apartment_parser.items import ApartmentItem
from apartment_parser.spiders.domovita.getters import (
    get_description,
    get_address,
)
from apartment_parser.spiders.domovita.setters import add_to_item


_LABEL_FIELD_DICT = {
    'Город': ('town', lambda x: x),
    'Этаж': ('floor', lambda x: int(x[1:-1])),
    'Год постройки': ('building_year', lambda x: int(x[1:5])),
    'Жилая площадь': ('square', lambda x: float(x[1 : x.index('м') - 1])),
    'Комнат': ('rooms', lambda x: int(x)),
    'Санузел': ('bathroom', lambda x: x[1:-1]),
    'Условия продажи': ('sale_conditions', lambda x: x[1:-1]),
}


class ApartmentDomovitaSpider(Spider):
    name = 'domovita'
    start_urls = ('https://domovita.by/minsk/flats/sale',)

    def parse(self, response: TextResponse, **kwargs) -> Generator:
        """
        This method parses list pages
        Gets links to detail pages on https://domovita.by and parses them
        When detail pages are over goes to next pageA
        """
        links_from_low = response.css('a.title--listing::attr(href)').extract()
        links_from_high = response.css('a.found_item::attr(href)').extract()
        links = links_from_high + links_from_low
        for link in links:
            yield response.follow(link, callback=self.parse_detail)

        next_page_li = response.css('li.page-item a::attr(href)').extract()
        if next_page_li[-1].split('=')[-1] == '2' or len(next_page_li) == 2:
            next_page = urljoin('https://domovita.by', next_page_li[-1])
            yield response.follow(next_page, callback=self.parse)

    def parse_detail(self, response: TextResponse, **kwargs) -> ApartmentItem:
        """
        Parse detail method
        Gets fields from detail page and sets them to item
        """
        item = ApartmentItem()
        photos_xpath = '//ul[@id="mainGalleryUpdate"]/li/img/@data-src'
        price_css = 'div.dropdown-pricechange_price-block div::text'
        description_css = 'div.white-space--pre-l p::text'
        item.photos = response.xpath(photos_xpath).extract()
        item.price = response.css(price_css).extract()[0]
        description_extracted = response.css(description_css).extract()
        item.description = get_description(description_extracted)
        parameters = response.css('div.object-info__parametr')
        for p in parameters:
            parameter = p.css('span')
            label = parameter[0].css('::text').extract()
            if 'Адрес' in label[0]:
                address_extracted = parameter[1].css('::text').extract()
                item.street, item.house = get_address(address_extracted)
                continue
            for key in _LABEL_FIELD_DICT:
                value = parameter[1].css('::text').extract()[0]
                if key in label[0]:
                    value = _LABEL_FIELD_DICT[key][1](value)
                    add_to_item(item, _LABEL_FIELD_DICT[key][0], value)
                    break
        return item
