def get_address(address_extracted: list, **kwargs) -> tuple:
    if len(address_extracted) == 2:
        street = address_extracted[0]
        house = address_extracted[1][5:-1]
    else:
        address = address_extracted[0].split(',')
        street = address[0][1:]
        house = address[1][4:-1]
    return street, house


def get_description(description_extracted: list) -> str:
    try:
        description = description_extracted[0]
    except IndexError:
        description = None
    return description
