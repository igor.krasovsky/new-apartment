from apartment_parser.items import ApartmentItem


def add_to_item(obj: ApartmentItem, attr: str, value: object):
    setattr(obj, attr, value)
