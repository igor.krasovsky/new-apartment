def format_town(town: str) -> str:
    return ' '.join(town.split('.')[1:]).strip()


def format_strip(s: str) -> str:
    return s.strip()


def format_square(square: str) -> float:
    value = square.split('/')[0].strip()
    try:
        formatted_value = float(value)
    except ValueError:
        formatted_value = None
    return formatted_value


def format_floor(floor: str) -> int:
    return int(floor.strip()[0])


def format_rooms(rooms_amount: str) -> int:
    try:
        rooms = int(rooms_amount.split('/')[0].strip())
    except ValueError:
        rooms = None
    return rooms
