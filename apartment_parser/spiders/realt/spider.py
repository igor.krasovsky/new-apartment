# -*- coding: utf-8 -*-

from typing import Generator

from scrapy.spiders import Spider
from scrapy.http.response.text import TextResponse

from apartment_parser.items import ApartmentItem
from apartment_parser.spiders.realt.getters import (
    get_price,
    get_sale_conditions,
    get_address,
    get_next_page,
)
from apartment_parser.spiders.realt.setters import add_to_item
from apartment_parser.spiders.realt.formatters import (
    format_town,
    format_floor,
    format_square,
    format_strip,
    format_rooms,
)


_LABEL_FIELD_DICT = {
    'Населенный пункт': ('town', 'a strong::text', format_town),
    'Этаж / этажность': ('floor', '::text', format_floor),
    'Площадь общая/жилая/кухня': ('square', 'strong::text', format_square),
    'Год постройки': ('building_year', '::text', format_strip),
    'Сан/узел': ('bathroom', '::text', format_strip),
    'Комнат всего/разд.': ('rooms', 'strong ::text', format_rooms),
}


class ApartmentRealtSpider(Spider):
    name = 'realt'
    start_urls = ('https://realt.by/sale/flats/',)

    def parse(self, response: TextResponse, **kwargs) -> Generator:
        """
        Parse list method
        Gets links and calls parse_detail on them for https://realt.by
        """
        links_div = response.css('div.listing')
        links = links_div.css('a.teaser-title::attr(href)').extract()
        for link in links:
            yield response.follow(link, callback=self.parse_detail)

        paging_list = response.css('div.paging-list a')
        next_page_link = get_next_page(paging_list)
        yield response.follow(next_page_link, self.parse)

    def parse_detail(self, response: TextResponse, **kwargs) -> ApartmentItem:
        item = ApartmentItem()
        price_css = 'div.select a::attr(data-price)'
        description_css = 'div.top-description p::text'
        sale_css = 'div.fs-small strong::text'
        photos_css = 'a.object-gallery-item img::attr(data-src)'
        item.price = get_price(response, price_css)
        item.sale_conditions = get_sale_conditions(response, sale_css)
        item.photos = response.css(photos_css).extract()
        item.description = ' '.join(response.css(description_css).extract())
        parameter_rows = response.css('table.table-params tr')
        for row in parameter_rows:
            row_label = row.css('td::text')[0].extract()
            if 'Адрес' in row_label:
                row_field = row.css('td')[1].css('::text').extract()
                item.street, item.house = get_address(row_field)
                continue
            for key in _LABEL_FIELD_DICT:
                if key in row_label:
                    row_field = row.css('td')[1]
                    field_value = row_field.css(_LABEL_FIELD_DICT[key][1])[0]
                    value = _LABEL_FIELD_DICT[key][2](field_value.extract())
                    add_to_item(item, _LABEL_FIELD_DICT[key][0], value)
                    break
        return item
