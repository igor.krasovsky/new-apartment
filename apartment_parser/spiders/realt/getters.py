from scrapy.http.response.text import TextResponse
from scrapy.selector.unified import SelectorList


def get_address(row_field: list) -> tuple:
    if len(row_field) == 1:
        splitted_address = row_field[0].strip().split(',')
        try:
            house = splitted_address[1].strip()[2:]
            street = ' '.join(splitted_address[0].strip().split(' ')[:-1])
        except IndexError:
            street = None
            house = splitted_address[0].strip()[2:]
        return street, house
    else:
        street = ' '.join(row_field[1].split(' ')[:-1])
        house = row_field[2].strip()[2:]
        return street, house


def get_sale_conditions(response: TextResponse, css: str) -> str:
    try:
        sale_conditions = response.css(css)[0].extract()
    except IndexError:
        sale_conditions = None
    return sale_conditions


def get_next_page(paging_list: SelectorList) -> str:
    i = 0
    print(type(paging_list))
    while i < len(paging_list):
        if 'active' in paging_list[i].css('a::attr(class)')[0].extract():
            i += 1
            break
        i += 1
    return paging_list[i].css('a::attr(href)').extract()[0]


def get_price(response: TextResponse, css: str) -> str:
    try:
        price = response.css(css)[0].extract()
    except IndexError:
        price = 'Договорная цена'
    return price
