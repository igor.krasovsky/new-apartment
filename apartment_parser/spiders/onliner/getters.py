def get_api_link(i: int) -> str:
    return (
        'https://pk.api.onliner.by/search/apartments?'
        'bounds[lb][lat]=53.69955212410502&bounds[lb]'
        '[long]=27.33261108398438&bounds[rt][lat]'
        '=54.09564422005199&bounds[rt][long]=27.79129028320313&'
        'page={0}&v=0.8647468729638831'.format(i)
    )


def get_address(address_line: str) -> tuple:
    address = address_line.split(',')
    try:
        town = address[-3].strip()
        street = address[-2].strip()
        house = address[-1].strip()
        return town, street, house
    except IndexError:
        return None, None, None


def get_photo_url(photo_style: str) -> str:
    start = photo_style.index('(') + 1
    return photo_style[start:-1]


def get_building_year(value: str) -> int:
    try:
        building_year = int(value.split(' ')[-2])
    except (ValueError, IndexError):
        building_year = None
    return building_year


def remove_next_line_symbol(s: str) -> str:
    return ''.join(s.split('\n'))


def get_description(tag: str) -> str:
    start = tag.index('>') + 1
    text = tag[start:-6]
    description = remove_next_line_symbol(''.join(text.split('<br>')))
    return description.strip()
