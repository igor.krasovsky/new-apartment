# -*- coding: utf-8 -*-

import json
from typing import Generator

from scrapy.http import Request
from scrapy.spiders import Spider
from scrapy.http.response.text import TextResponse

from apartment_parser.items import ApartmentItem
from apartment_parser.spiders.onliner.getters import (
    get_api_link,
    get_address,
    get_description,
    get_photo_url,
    get_building_year,
)


class ApartmentOnlinerSpider(Spider):
    name = 'onliner'
    current_page_id = 1
    start_urls = (get_api_link(1),)

    def parse(self, response: TextResponse, **kwargs) -> Generator:
        text_api = json.loads(response.text)
        for detail in text_api['apartments']:
            yield self.parse_json(detail)
        self.current_page_id += 1
        if self.current_page_id < 9:
            next_link = get_api_link(self.current_page_id)
            yield response.follow(next_link, callback=self.parse)

    def parse_json(self, detail_api: dict, **kwargs) -> Request:
        item = ApartmentItem()
        address_line = detail_api['location']['address']
        detail_page_url = detail_api['url']
        request = Request(detail_page_url, callback=self.parse_detail)
        request.meta['item'] = item
        item.rooms = detail_api['number_of_rooms']
        item.floor = detail_api['floor']
        item.price = detail_api['price']['converted']['BYN']['amount'] + ' BYN'
        item.square = detail_api['area']['total']
        item.town, item.street, item.house = get_address(address_line)
        return request

    def parse_detail(self, response: TextResponse, **kwargs) -> ApartmentItem:
        item = response.meta['item']
        description_css = 'div.apartment-info__sub-line_extended-bottom'
        description_tag = response.css(description_css)[0].extract()
        building_year_css = 'li.apartment-options__item::text'
        building_year_label = response.css(building_year_css)[1].extract()
        photos_css = 'div.apartment-cover__thumbnails-inner div::attr(style)'
        photo_styles = response.css(photos_css).extract()
        item.description = get_description(description_tag)
        item.building_year = get_building_year(building_year_label)
        item.photos = [get_photo_url(x) for x in photo_styles]
        return item
