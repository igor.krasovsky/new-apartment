import datetime

from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.sql.functions import coalesce
from sqlalchemy.engine import Engine

from common.alchemy_tables import Address, Photo, Apartment


def insert_address(db: Engine, item: dict, location: tuple) -> None:
    statement = (
        insert(Address)
        .values(
            longitude=location[0],
            latitude=location[1],
            town=item['town'],
            street=item['street'],
            house=item['house'],
        )
        .on_conflict_do_nothing()
    )
    db.execute(statement)


def insert_apartment(db: Engine, item: dict, address_id: int) -> None:
    statement = insert(Apartment).values(
        price=item['price'],
        building_year=item['building_year'],
        floor=item['floor'],
        square=item['square'],
        rooms=item['rooms'],
        description=item['description'],
        bathroom=item['bathroom'],
        sale_conditions=item['sale_conditions'],
        created_at=datetime.datetime.now(),
        updated_at=datetime.datetime.now(),
        address_id=address_id,
    )
    instruction = statement.on_conflict_do_update(
        index_elements=[
            'address_id',
        ],
        set_=dict(
            price=coalesce(statement.excluded.price, Apartment.c.price),
            building_year=coalesce(
                statement.excluded.building_year, Apartment.c.building_year
            ),
            floor=coalesce(statement.excluded.floor, Apartment.c.floor),
            square=coalesce(statement.excluded.square, Apartment.c.square),
            rooms=coalesce(statement.excluded.rooms, Apartment.c.rooms),
            description=coalesce(
                statement.excluded.description, Apartment.c.description
            ),
            bathroom=coalesce(
                statement.excluded.bathroom, Apartment.c.bathroom
            ),
            sale_conditions=coalesce(
                statement.excluded.sale_conditions, Apartment.c.sale_conditions
            ),
            updated_at=datetime.datetime.now(),
        ),
    )
    db.execute(instruction)


def insert_photos(db: Engine, item: dict, apartment_id: int):
    for photo in item['photos']:
        statement = insert(Photo).values(
            image=photo, apartment_id=apartment_id
        )
        db.execute(statement)
