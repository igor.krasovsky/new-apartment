from sqlalchemy import create_engine

from common.env_reader import get_environ
from common.alchemy_tables import metadata


class TestDatabase:
    def __init__(self):
        env = get_environ()
        self._metadata = metadata
        self._creator = create_engine(
            env.str('SETUP_TEST_DATABASE_OPTIONS'),
            isolation_level='AUTOCOMMIT',
        )
        self.create_test_db()
        self._db = create_engine(env.str('TEST_DATABASE_OPTIONS'))
        self._metadata.create_all(self._db)

    def create_test_db(self):
        self._creator.execute('CREATE DATABASE test_db_apartment;')

    def get_engine(self):
        return self._db

    def drop_test_db(self):
        self._db.dispose()
        self._creator.execute('DROP DATABASE test_db_apartment;')
        self._creator.dispose()
