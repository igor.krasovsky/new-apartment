import pytest

from workers.getters import get_apartment_id, get_address_id
from workers.modifiers import insert_address, insert_apartment


class TestGetters:
    def test_get_address_id(self, database_with_address, item):
        address_id = get_address_id(database_with_address, item)
        assert address_id == 1

    def test_get_apartment_id(self, database_with_apartment, item):
        address_id = get_address_id(database_with_apartment, item)
        apartment_id = get_apartment_id(database_with_apartment, address_id)
        assert apartment_id == 1
