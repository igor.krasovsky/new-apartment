import pytest

from workers.modifiers import insert_address, insert_apartment, insert_photos
from workers.getters import get_apartment_id, get_address_id


class TestModifiers:
    def test_insert_address(self, database, item):
        location = (item['longitude'], item['latitude'])
        insert_address(database, item, location)
        count = database.execute(
            'SELECT COUNT(*) FROM apartments_address'
        ).fetchall()[0][0]
        assert count == 1

    def test_insert_address_duplicate(self, database, item):
        location = (item['longitude'], item['latitude'])
        insert_address(database, item, location)
        insert_address(database, item, location)
        count = database.execute(
            'SELECT COUNT(*) FROM apartments_address'
        ).fetchall()[0][0]
        assert count == 1

    def test_insert_apartment(self, database_with_address, item):
        address_id = get_address_id(database_with_address, item)
        insert_apartment(database_with_address, item, address_id)
        count = database_with_address.execute(
            'SELECT COUNT(*) FROM apartments_apartment;'
        ).fetchall()[0][0]
        assert count == 1

    def test_insert_apartment_duplicate(self, database_with_address, item):
        address_id = get_address_id(database_with_address, item)
        insert_apartment(database_with_address, item, address_id)
        item['building_year'] = 1700
        insert_apartment(database_with_address, item, address_id)
        count = database_with_address.execute(
            'SELECT COUNT(*) FROM apartments_apartment;'
        ).fetchall()[0][0]
        building_year = database_with_address.execute(
            'SELECT building_year FROM apartments_apartment;'
        ).fetchall()[0][0]
        assert count == 1
        assert building_year == 1700

    def test_insert_photos(self, database_with_apartment, item):
        address_id = get_address_id(database_with_apartment, item)
        apartment_id = get_apartment_id(database_with_apartment, address_id)
        insert_photos(database_with_apartment, item, apartment_id)
        count = database_with_apartment.execute(
            'SELECT COUNT(*) FROM apartments_photo;'
        ).fetchall()[0][0]
        assert count == len(item['photos'])
