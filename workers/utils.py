from geopy import Nominatim


def get_location(item: dict) -> tuple:
    locator = Nominatim(user_agent='myGeocoder')
    address_list = [item['street'], item['house'], item['town']]
    location_address = ', '.join(address_list)
    location = locator.geocode(location_address)
    if location:
        return location.longitude, location.latitude
    else:
        return ()
