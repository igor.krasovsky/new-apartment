import pytest
from factory import DictFactory, List
from factory.fuzzy import FuzzyText, FuzzyInteger, FuzzyFloat

from workers.tests.test_db import TestDatabase
from workers.modifiers import insert_address, insert_apartment, insert_photos
from workers.getters import get_address_id, get_apartment_id


class ItemFactory(DictFactory):
    town = FuzzyText()
    street = FuzzyText()
    house = FuzzyText()
    floor = FuzzyInteger(1, 50)
    price = FuzzyText()
    building_year = FuzzyInteger(1900, 2021)
    square = FuzzyFloat(10, 1000)
    rooms = FuzzyInteger(1, 7)
    description = FuzzyText()
    bathroom = FuzzyText()
    sale_conditions = FuzzyText()
    longitude = FuzzyFloat(-180, 180)
    latitude = FuzzyFloat(-90, 90)
    photos = List([FuzzyText() for _ in range(10)])


@pytest.fixture
def database():
    db = TestDatabase()
    yield db.get_engine()
    db.drop_test_db()


@pytest.fixture
def database_with_address(item):
    db = TestDatabase()
    engine = db.get_engine()
    location = (item['longitude'], item['latitude'])
    insert_address(engine, item, location)
    yield engine
    db.drop_test_db()


@pytest.fixture
def database_with_apartment(item):
    db = TestDatabase()
    engine = db.get_engine()
    location = (item['longitude'], item['latitude'])
    insert_address(engine, item, location)
    address_id = get_address_id(engine, item)
    insert_apartment(engine, item, address_id)
    yield engine
    db.drop_test_db()


@pytest.fixture
def item():
    return ItemFactory()
