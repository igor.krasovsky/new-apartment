from json import loads, dumps

from common.rabbitmq_connector import RabbitMQConnector
from common.postrgresql_db import PostgreSQLDB
from common.alchemy_tables import metadata
from common.redis_connector import RedisConnector
from common.env_reader import get_environ
from workers.modifiers import (
    insert_address,
    insert_apartment,
    insert_photos,
)
from workers.getters import get_address_id, get_apartment_id


env = get_environ()


def callback(rabbitmq_channel, method, properties, body) -> None:
    item = loads(body)
    location = (15, 15)
    if item['town'] and item['street'] and item['house'] and location:
        db = PostgreSQLDB()
        engine = db.get_engine()
        metadata.create_all(engine, checkfirst=True)
        insert_address(engine, item, location)
        address_id = get_address_id(engine, item)
        insert_apartment(engine, item, address_id)
        apartment_id = get_apartment_id(engine, address_id)
        insert_photos(engine, item, apartment_id)
    else:
        redis_connector = RedisConnector()
        redis_connector.push(
            dumps(item),
            expiration_time=env.int('REDIS_EXPIRATION_TIME') * 3600,
        )


if __name__ == '__main__':
    connector = RabbitMQConnector()
    connector.channel_consume(callback)
