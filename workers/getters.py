from sqlalchemy import select
from sqlalchemy.engine import Engine

from common.alchemy_tables import Address, Apartment


def get_address_id(db: Engine, item):
    instruction = (
        select(
            [
                Address.c.id,
            ]
        )
        .where(Address.c.town == item['town'])
        .where(Address.c.street == item['street'])
        .where(Address.c.house == item['house'])
    )
    return db.execute(instruction).fetchall()[0][0]


def get_apartment_id(db: Engine, address_id: int):
    instruction = select(
        [
            Apartment.c.id,
        ]
    ).where(Apartment.c.address_id == address_id)
    return db.execute(instruction).fetchall()[0][0]
